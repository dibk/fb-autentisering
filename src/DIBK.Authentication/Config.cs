// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using IdentityServer4;
using IdentityServer4.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DIBK.Authentication
{
    public class ClientRepo
    {
        private readonly IConfiguration _configuration;

        public ClientRepo(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IEnumerable<Client> GetClients()
        {
            var clients = new List<Client>();

            var configuredClients = GetClientsFromConfiguration();
            if (configuredClients != null && configuredClients.Count > 0)
            {
                foreach (var item in GetClientsFromConfiguration())
                {
                    ICollection<string> allowedGrantTypes = new List<string>();
                    if (item.AllowedGrantTypes.Equals("Implicit", StringComparison.OrdinalIgnoreCase))
                        allowedGrantTypes = GrantTypes.Implicit;
                    else
                        allowedGrantTypes = GrantTypes.Code;

                    var client = new Client()
                    {
                        ClientId = item.ClientId,
                        ClientName = item.ClientName,
                        AllowedGrantTypes = allowedGrantTypes,
                        AllowedScopes = {
                        "DIBK.Checklist.Api.Internal",
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email },
                        AllowOfflineAccess = true,
                        RequireClientSecret = false,
                        RequireConsent = false,
                        AllowAccessTokensViaBrowser = item.AllowAccessTokensViaBrowser,

                        RedirectUris = item.RedirectUris,
                        PostLogoutRedirectUris = item.PostLogoutRedirectUris,
                    };

                    clients.Add(client);
                }
            }
            else
                clients = IdentityServerConfig.Clients.ToList();

            return clients;
        }

        private List<ConfigClient> GetClientsFromConfiguration()
        {
            return _configuration.GetSection("Clients").Get<List<ConfigClient>>();
        }
    }

    public class ConfigClient
    {
        public string ClientId { get; set; }
        public string ClientName { get; set; }
        public string AllowedGrantTypes { get; set; }
        public bool AllowAccessTokensViaBrowser { get; set; }
        public List<string> RedirectUris { get; set; }
        public List<string> PostLogoutRedirectUris { get; set; }
    }

    public static class IdentityServerConfig
    {
        public static IEnumerable<IdentityResource> Ids =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()
            };

        public static IEnumerable<ApiResource> Apis =>
        new List<ApiResource>
        {
            new ApiResource("DIBK.Checklist.Api.Internal", "DIBK.Checklist.Api.Internal"),
        };

        public static IEnumerable<Client> Clients =>
            new List<Client>
            {
                new Client
                { ClientId ="DIBK.Checklist",
                    ClientName = "DIBK Checklist",
                    AllowedGrantTypes = GrantTypes.Code,
                    AllowedScopes = {
                        "DIBK.Checklist.Api.Internal",
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email },
                    AllowOfflineAccess = true,
                    RequireClientSecret = false, //TODO: check if needed

                    //No consent page
                    RequireConsent = false,

                    //Redirection after login -- TODO make configurable
                    RedirectUris = {
                        //Local test
                        "http://localhost:3000/signin-oidc",
                        "http://localhost:8000/signin-oidc",
                        //Bygg
                        "https://dibkchecklist.azurewebsites.net/signin-oidc",
                        "https://ftb-checklist.dibk.no/signin-oidc",
                        "https://sjekkliste-bygg.ft-dev.dibk.no/signin-oidc",
                        "https://sjekkliste-bygg.ft-test.dibk.no/signin-oidc",
                        //Arbeidstilsynet
                        "https://arbeidstilsynet-checklist.azurewebsites.net/signin-oidc",
                        "https://sjekkliste-arbeidstilsynet.dibk.net/signin-oidc",
                        "https://sjekkliste-arbeidstilsynet.ft-dev.dibk.no/signin-oidc",
                        "https://sjekkliste-arbeidstilsynet.ft-test.dibk.no/signin-oidc",
                        //Trappesjekk
                        "https://dibk-tekcheck.azurewebsites.net/signin-oidc",
                        "https://dibk-trappetek.azurewebsites.net/signin-oidc",
                        "https://trappesjekk.dibk.no/signin-oidc",
                        "https://trappesjekk.ft-dev.dibk.no/signin-oidc",
                        "https://trappesjekk.ft-test.dibk.no/signin-oidc",
                        //Plan
                        "https://plan-checklist-dev.azurewebsites.net/signin-oidc",
                        "https://plan-checklist-test.azurewebsites.net/signin-oidc",
                        "https://sjekkliste-plan.ft-dev.dibk.no/signin-oidc",
                        "https://sjekkliste-plan.ft-test.dibk.no/signin-oidc",
                        //Forvaltning planbestemmelser                        
                        "https://forvaltningavplanbestemmelser.ft-dev.dibk.no/signin-oidc",
                        "https://forvaltningavplanbestemmelser.ft-test.dibk.no/signin-oidc",
                        "https://forvaltningavplanbestemmelser.ft.dibk.no/signin-oidc",
                    },

                    //Redirection after logout -- TODO make configurable
                    PostLogoutRedirectUris = {
                        //Local test
                        "http://localhost:3000/signout-callback-oidc",
                        "http://localhost:8000/signout-callback-oidc",
                        //Bygg
                        "https://dibkchecklist.azurewebsites.net/signout-callback-oidc",
                        "https://ftb-checklist.dibk.no/signout-callback-oidc",
                        "https://sjekkliste-bygg.ft-dev.dibk.no/signout-callback-oidc",
                        "https://sjekkliste-bygg.ft-test.dibk.no/signout-callback-oidc",
                        //Arbeidstilsynet
                        "https://arbeidstilsynet-checklist.azurewebsites.net/signout-callback-oidc",
                        "https://atil-checklist.dibk.net/signout-callback-oidc",
                        "https://sjekkliste-arbeidstilsynet.ft-dev.dibk.no/signout-callback-oidc",
                        "https://sjekkliste-arbeidstilsynet.ft-test.dibk.no/signout-callback-oidc",
                        //Trappesjekk
                        "https://dibk-tekcheck.azurewebsites.net/signout-callback-oidc",
                        "https://dibk-trappetek.azurewebsites.net/signout-callback-oidc",
                        "https://trappesjekk.dibk.no/signout-callback-oidc",
                        "https://trappesjekk.ft-dev.dibk.no/signout-callback-oidc",
                        "https://trappesjekk.ft-test.dibk.no/signout-callback-oidc",
                        //Plan
                        "https://plan-checklist-test.dibk.net/signout-callback-oidc",
                        "https://sjekkliste-plan.ft-dev.dibk.no/signout-callback-oidc",
                        "https://sjekkliste-plan.ft-test.dibk.no/signout-callback-oidc",
                        //Forvaltning planbestemmelser
                        "https://forvaltningavplanbestemmelser.ft-dev.dibk.no/signout-callback-oidc",
                        "https://forvaltningavplanbestemmelser.ft-test.dibk.no/signout-callback-oidc",
                        "https://forvaltningavplanbestemmelser.ft.dibk.no/signout-callback-oidc",
                    },
                },
                new Client
                {
                    ClientId ="DIBK.Checklist.Postman",
                    ClientName = "DIBK Checklist Postman",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,
                    AllowedScopes = {
                        "DIBK.Checklist.Api.Internal",
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email },
                    AllowOfflineAccess = true,
                    RequireClientSecret = false, //TODO: check if needed

                    //No consent page
                    RequireConsent = false,

                    //Redirection after login -- TODO make configurable
                    RedirectUris = {
                        // ?? Ska det pekes p� Frontend eller Backend her?? Litt blandings ser det ut til...

                        //Local test
                        "http://localhost:5020/oauth2/callback",
                        //Bygg
                        "https://dibk-checklist-api.azurewebsites.net/oauth2/callback",
                        "https://ftb-checklist-api.dibk.no/oauth2/callback",
                        "https://sjekkliste-bygg.ft-dev.dibk.no/oauth2/callback",
                        "https://sjekkliste-bygg.ft-test.dibk.no/oauth2/callback",
                        //Arbeidstilsynet
                        "https://arbeidstilsynet-checklist-api.azurewebsites.net/oauth2/callback",
                        "https://atil-checklist-api.dibk.net/oauth2/callback",
                        "https://sjekkliste-arbeidstilsynet.ft-dev.dibk.no/oauth2/callback",
                        "https://sjekkliste-arbeidstilsynet.ft-test.dibk.no/oauth2/callback",
                        //Trappesjekk
                        "https://trappesjekk.ft-dev.dibk.no/oauth2/callback",
                        "https://trappesjekk.ft-test.dibk.no/oauth2/callback",
                        //Plan
                        "https://plan-checklist-api-test.azurewebsites.net/oauth2/callback",
                        "https://sjekkliste-plan.ft-dev.dibk.no/oauth2/callback",
                        "https://sjekkliste-plan.ft-test.dibk.no/oauth2/callback",
                        //Forvaltning planbestemmelser
                        "https://forvaltningavplanbestemmelser.ft-dev.dibk.no/oauth2/callback",
                        "https://forvaltningavplanbestemmelser.ft-test.dibk.no/oauth2/callback",
                        "https://forvaltningavplanbestemmelser.ft.dibk.no/oauth2/callback"
                    },

                    //Redirection after logout -- TODO make configurable
                    PostLogoutRedirectUris = {
                    },
                },
            };
    }
}