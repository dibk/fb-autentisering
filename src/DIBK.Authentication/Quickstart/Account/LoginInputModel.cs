// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using System.ComponentModel.DataAnnotations;

namespace fb_autentisering
{
    public class LoginInputModel
    {
        [Required(ErrorMessage = "E-post er p�krevd")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Passord er p�krevd")]
        public string Password { get; set; }
        public bool RememberLogin { get; set; }
        public string ReturnUrl { get; set; }
    }
}