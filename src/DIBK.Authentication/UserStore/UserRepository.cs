﻿using IdentityModel;
using IdentityServer4.Test;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Security.Claims;

namespace DIBK.Authentication.UserStore
{
    public class UserRepository
    {
        private readonly IConfiguration _configuration;

        public UserRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public List<TestUser> GetUsers()
        {
            var users = new List<TestUser>();

            foreach (var userStoreItem in GetUsersFromTempStore())
            {
                users.Add(MapToTestUser(userStoreItem));
            }

            return users;
        }

        private List<UserFromTempStore> GetUsersFromTempStore()
        {            
            return _configuration.GetSection("ChecklistUsers").Get<List<UserFromTempStore>>();
        }

        private TestUser MapToTestUser(UserFromTempStore userFromTempStore)
        {
            var testUser = new TestUser() { SubjectId = userFromTempStore.SubjectId, Username = userFromTempStore.Username, Password = userFromTempStore.Password };

            testUser.Claims.Add(new Claim(JwtClaimTypes.Name, userFromTempStore.Name));
            testUser.Claims.Add(new Claim(JwtClaimTypes.Email, userFromTempStore.Email));
            testUser.Claims.Add(new Claim(JwtClaimTypes.EmailVerified, userFromTempStore.EmailVerified.ToString()));

            return testUser;
        }
    }
}
