﻿using System;
using System.Collections.Generic;

namespace DIBK.Authentication.UserStore
{
    public class UserFromTempStore
    {
        public string SubjectId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool EmailVerified { get; set; }
    }
}
